from django.shortcuts import render, redirect
from django.core import serializers
from django.http import HttpResponse, JsonResponse
from .models import *
# Create your views here.


def index(request):
	return render(request, "app4/rekomendasi.html")

def update_avg(items):
	total = 0
	for i in items:
		total += i.poll

	for i in items:
		try:
			i.avg = round(i.poll / total * 100)
		except:
			i.avg = 0
		i.save()

# def model(request, model):
# 	objects = 0
# 	templates = ''
# 	if model == "Games":
# 		objects = Games.objects.all()
# 		templates = 'app/games.html'
# 	elif model == "Movies":
# 		objects = Movies.objects.all()
# 		templates = 'app/movies.html'
# 	elif model == "Music":
# 		objects = Music.objects.all()
# 		templates = 'app/musics.html'
# 	elif model == "Podcast":
# 		objects = Podcast.objects.all()
# 		templates = 'app/podcast.html'

# 	total = 0
# 	for o in objects:
# 		total += o.poll

# 	for o in objects:
# 		o.avg = round(o.poll / total * 100)
# 		o.save()

# 	data={
# 		'object':games,
# 		'model' : 'games',
# 		'cek' : request.COOKIES.get('poll_games'),
# 		'name': request.COOKIES.get('name'),

def games(request):
	games = Games.objects.all().order_by('id')
	total = 0
	update_avg(games)

	data={
		# 'object':serializers.serialize('json',games),
		'object' : games,
		'model' : 'games',
		'cek' : request.COOKIES.get('poll_games'),
		'name': request.COOKIES.get('name'),
	}

	# return JsonResponse(data)

	return render(request, "app4/games.html", data)

def music(request):
	music = Music.objects.all().order_by('id')
	total = 0
	update_avg(music)

	data={
		'object':music,
		'model' : 'music',
		'cek' : request.COOKIES.get('poll_music'),
		'name': request.COOKIES.get('name'),
	}

	return render(request, "app4/musics.html", data)

def movies(request):
	movies = Movies.objects.all().order_by('id')
	total = 0
	update_avg(movies)

	data={
		'object':movies,
		'model' : 'movies',
		'cek' : request.COOKIES.get('poll_movies'),
		'name': request.COOKIES.get('name'),
	}

	return render(request, "app4/movies.html", data)	

def podcast(request):
	podcast = Podcast.objects.all().order_by('id')
	total = 0
	update_avg(podcast)

	data={
		'object':podcast,
		'model' : 'podcast',
		'cek' : request.COOKIES.get('poll_podcast'),
		'name': request.COOKIES.get('name'),
	}

	return render(request, "app4/podcast.html", data)

def poll(request, model):
	ret = {}
	poll = Poll.objects.get(user=request.user)
	if request.POST:
		id = request.POST.get('item')
		if model == 'games':
			item = Games.objects.get(id=id)
			poll.games = True
			items = Games.objects.all().order_by('id')

		if model == 'movies':
			item = Movies.objects.get(id=id)
			poll.movies = True
			items = Movies.objects.all().order_by('id')

		if model == 'music':
			item = Music.objects.get(id=id)
			poll.music = True
			items = Music.objects.all().order_by('id')

		if model == 'podcast':
			item = Podcast.objects.get(id=id)
			poll.podcast = True
			items = Podcast.objects.all().order_by('id')


		item.Poll()
		poll.save()
		update_avg(items)
		ret['object'] = serializers.serialize('json',items)

	ret['cek'] = getattr(poll, model)
	# print(ret)
	return JsonResponse(ret)

	
