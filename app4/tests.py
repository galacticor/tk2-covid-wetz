from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest, JsonResponse
from .views import *
from .models import *

# Create your tests here.
class TestGeneral(TestCase):
	def test_event_url_is_exist(self):
		response = Client().get("/recommendation/")
		self.assertEqual(response.status_code, 200)


class TestGames(TestCase):
	def setUp(self):
		a = Games(name="abc")
		a.save()

	def testPoll(self):
		a = Games.objects.get(id=1)
		a.Poll()
		self.assertEqual(a.poll, 1)

	def test_printModel(self):
		a = Games.objects.get(id=1)
		self.assertEqual(str(a), "1 - abc")

	def test_event_url_is_exist(self):
		a = Games(name="a")
		a.save()
		response = Client().get("/recommendation/games/")
		self.assertEqual(response.status_code, 200)

	# def test_poll_url_redirect_is_exist(self):
	# 	data = {
	# 		'item': 1,
	# 	}
	# 	response = self.client.post('/recommendation/games/poll', data)
	# 	if isinstance(response, JsonResponse):
	# 		self.assertEqual(1, 1)
	# 	else:
	# 		self.assertEqual(1, 0)

class TestMovies(TestCase):
	def setUp(self):
		a = Movies(name="abc")
		a.save()

	def testPoll(self):
		a = Movies.objects.get(id=1)
		a.Poll()
		self.assertEqual(a.poll, 1)

	def test_printModel(self):
		a = Movies.objects.get(id=1)
		self.assertEqual(str(a), "1 - abc")

	def test_event_url_is_exist(self):
		response = Client().get("/recommendation/movies/")
		self.assertEqual(response.status_code, 200)

	# def test_poll_url_redirect_is_exist(self):
	# 	data = {
	# 		'item': 1,
	# 	}
	# 	response = self.client.post('/recommendation/movies/poll', data)
	# 	if isinstance(response, JsonResponse):
	# 		self.assertEqual(1, 1)
	# 	else:
	# 		self.assertEqual(1, 0)
class TestMusic(TestCase):
	def setUp(self):
		a = Music(name="abc")
		a.save()

	def testPoll(self):
		a = Music.objects.get(id=1)
		a.Poll()
		self.assertEqual(a.poll, 1)

	def test_printModel(self):
		a = Music.objects.get(id=1)
		self.assertEqual(str(a), "1 - abc")

	def test_event_url_is_exist(self):
		response = Client().get("/recommendation/music/")
		self.assertEqual(response.status_code, 200)

	# def test_poll_url_redirect_is_exist(self):
	# 	data = {
	# 		'item': 1,
	# 	}
	# 	response = self.client.post('/recommendation/music/poll', data)
	# 	if isinstance(response, JsonResponse):
	# 		self.assertEqual(1, 1)
	# 	else:
	# 		self.assertEqual(1, 0)

class TestPodcast(TestCase):
	def setUp(self):
		a = Podcast(name="abc")
		a.save()

	def testPoll(self):
		a = Podcast.objects.get(id=1)
		a.Poll()
		self.assertEqual(a.poll, 1)

	def test_printModel(self):
		a = Podcast.objects.get(id=1)
		self.assertEqual(str(a), "1 - abc")

	def test_event_url_is_exist(self):
		response = Client().get("/recommendation/podcast/")
		self.assertEqual(response.status_code, 200)

	# def test_poll_url_response_json_exist(self):
	# 	data = {
	# 		'item': 1,
	# 	}
	# 	response = self.client.post('/recommendation/podcast/poll', data)
	# 	if isinstance(response, JsonResponse):
	# 		self.assertEqual(1, 1)
	# 	else:
	# 		self.assertEqual(1, 0)

class TestGuest(TestCase):
	def test_cant_poll(self):
		r = Client().get('/recommendation/games/')
		html_response = r.content.decode('utf8')
		self.assertIn("Hi there, please login to vote our recommendation.", html_response)

		r = Client().get('/recommendation/movies/')
		html_response = r.content.decode('utf8')
		self.assertIn("Hi there, please login to vote our recommendation.", html_response)

		r = Client().get('/recommendation/music/')
		html_response = r.content.decode('utf8')
		self.assertIn("Hi there, please login to vote our recommendation.", html_response)

		r = Client().get('/recommendation/podcast/')
		html_response = r.content.decode('utf8')
		self.assertIn("Hi there, please login to vote our recommendation.", html_response)

class TestUser(TestCase):
	def setUp(self):
		user = User.objects.create_user(
			username="username",
			first_name="username",
			email="user@email.com",
			password="user_password",
			)

		user.save()
		poll = Poll(user=user)
		poll.save()
		a = Podcast(name="abc")
		a.save()
		a = Games(name="abc")
		a.save()
		a = Movies(name="abc")
		a.save()
		a = Music(name="abc")
		a.save()

	def test_hi_username(self):
		self.client.login(username="username", password="user_password")

		r1 = self.client.get('/recommendation/games/')
		html_r1 = r1.content.decode('utf8')
		self.assertIn("Hi username, let's vote our recommendation.", html_r1)

		r1 = self.client.get('/recommendation/movies/')
		html_r1 = r1.content.decode('utf8')
		self.assertIn("Hi username, let's vote our recommendation.", html_r1)

		r1 = self.client.get('/recommendation/podcast/')
		html_r1 = r1.content.decode('utf8')
		self.assertIn("Hi username, let's vote our recommendation.", html_r1)

		r1 = self.client.get('/recommendation/music/')
		html_r1 = r1.content.decode('utf8')
		self.assertIn("Hi username, let's vote our recommendation.", html_r1)
		
	def test_can_poll(self):
		self.client.login(username="username", password="user_password")

		response = self.client.get('/recommendation/games/poll')
		html_response = response.content.decode('utf8')
		self.assertNotIn("Sorry, you have to log in to use this feature", html_response)

	def test_response_json(self):
		self.client.login(username="username", password="user_password")
		data = {
			'item' : 1,
		}
		response = self.client.post('/recommendation/podcast/poll', data)
		self.assertEqual(True, isinstance(response, JsonResponse))

		response = self.client.post('/recommendation/music/poll',data)
		self.assertEqual(True, isinstance(response, JsonResponse))
		
		response = self.client.post('/recommendation/movies/poll', data)
		self.assertEqual(True, isinstance(response, JsonResponse))

		response = self.client.post('/recommendation/games/poll',data)
		self.assertEqual(True, isinstance(response, JsonResponse))
