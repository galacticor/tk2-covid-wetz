from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Games(models.Model):
	name = models.CharField(max_length=120, unique=True)
	desc = models.TextField(blank=True, null=True)
	poll = models.PositiveIntegerField(default=0)
	avg = models.PositiveIntegerField(default=0)

	def __str__(self):
		return "{} - {}".format(self.id, self.name)

	def Poll(self):
		self.poll += 1
		self.save()


class Music(models.Model):
	name = models.CharField(max_length=120, unique=True)
	desc = models.TextField(blank=True, null=True)
	poll = models.PositiveIntegerField(default=0)
	avg = models.PositiveIntegerField(default=0)

	def __str__(self):
		return "{} - {}".format(self.id, self.name)

	def Poll(self):
		self.poll += 1
		self.save()


class Movies(models.Model):
	name = models.CharField(max_length=120, unique=True)
	desc = models.TextField(blank=True, null=True)
	poll = models.PositiveIntegerField(default=0)
	avg = models.PositiveIntegerField(default=0)

	def __str__(self):
		return "{} - {}".format(self.id, self.name)

	def Poll(self):
		self.poll += 1
		self.save()


class Podcast(models.Model):
	name = models.CharField(max_length=120, unique=True)
	desc = models.TextField(blank=True, null=True)
	poll = models.PositiveIntegerField(default=0)
	avg = models.PositiveIntegerField(default=0)

	def __str__(self):
		return "{} - {}".format(self.id, self.name)

	def Poll(self):
		self.poll += 1
		self.save()

class Poll(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
	games = models.BooleanField(default=False)
	movies = models.BooleanField(default=False)
	podcast = models.BooleanField(default = False)
	music = models.BooleanField(default = False)