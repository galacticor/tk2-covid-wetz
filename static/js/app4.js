$(function(){
	$.ajax({
		url: "poll",
		success: function(data){
			console.log(data);
			if (data['cek'] == true){
				$("#poll").css("display", "none");
				$("#result-poll").css("display", "table")
			}
			else{
				$(".result-poll-wrapper").empty()
			}
		}
	})

	$("#do-poll").click(function() {
		$('html, body').animate({
			scrollTop: ($('.poll-msg').offset().top)
		},1000);
	});

	function getFormData($form){
	    var unindexed_array = $form.serializeArray();
	    var indexed_array = {};

	    $.map(unindexed_array, function(n, i){
	        indexed_array[n['name']] = n['value'];
	    });

	    return indexed_array;
	}

	$("form").submit( function(e){
		e.preventDefault();
		alert("submit")
		$("#poll").css("display", "none");
		$("#result-poll").css("display", "table")
		var $form = $('form');
		var input = getFormData($form);
		$(".result-poll-wrapper").empty()
		$.ajax({
			headers: { "X-CSRFToken": input['csrfmiddlewaretoken'] },
			url: "poll",
			type: "post",
			data:{
				'item' : input['item']
			},
			dataType: 'json',
			success: function(data){
				console.log(data);
				var content = "";
				for (var x of JSON.parse(data['object'])){
					content += `
					<div class="result-poll-item">
						<h3 class="result-poll-label">${x.fields.name}</h3>
						<div class="progress">
							<div class="progress-bar" role="progressbar" style="width: ${x.fields.avg}%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">${x.fields.avg}%</div>
						</div>
					</div>
					`
				}
				$(".result-poll-wrapper").append(content);
			}
		})
	})
});