$(document).ready(function(){
    $('#blog-form').on('submit', function(e){
        e.preventDefault();
        console.log('Blog added!');
        add_blog();
    })
    
    $('#like-btn').click(function(e){
        e.preventDefault();
        //console.log('Blog liked!')
        blog_like();
    })

    $('#comment-form').on('submit', function(e){
        e.preventDefault();
        //alert("Comment submitted!");
        console.log('Comment submitted!');
        add_comment();
    })

    $('.fieldset-disable-forms').prop('disabled', true);
    
    function add_blog(){
        console.log('Add blog is working!')
        var bform = $("#blog-form");
        var url_add_blog = bform.attr('url_path');
        //console.log(url_add_blog)

        $.ajax({
            url: url_add_blog,
            type: "POST",
            data: {
                title : $('#blog-title').val(),
                content : $('#blog-content').val(),
                csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val(),
            },

            success: function(json) {
                //console.log(json)
                var blogs = json.dataBlog[0];
                $('#blog-title').val('');
                $('#blog-content').val('');
                $('.list-blog').empty();
                $('.blog-warn').empty();
                $('.blog-warn').append(
                    `<h1 class="our-story">Here's <span class="bold-red">our story.</span> By <span class="bold-red">yours.</span></h1>`
                );
                $('.success-blog').append(
                    `<div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <h5>Blog added <strong style="font-size:inherit;">successfully!</strong></h5>
                    </div>`
                );
                for(var blog in blogs){
                    var shortText = jQuery.trim(blogs[blog][1]).substring(0, 100).trim(this) + " ...";
                    $('.list-blog').append(
                        `<div class="col-lg-4 d-flex align-items-stretch">
                            <div class="card h-100 w-100">
                                <h3 class="card-title h-100">` + blogs[blog][0] + `</h3>
                                <p class="card-text h-100">` + shortText + `</p>
                                <div class="button-more">
                                    <a class="btn btn-lg" href="details/` + blogs[blog][0] + `" role="button">More</a>
                                </div>
                            </div>
                        </div>`
                    );
                }
            }
        })
    }

    function blog_like(){
        console.log("Blog like is working!");
        var form = $('#like-submit');
        var url_like = form.attr('url_path');
        //console.log(url_like);
        $.ajax({
            url: url_like,
            type : "POST",
            data: {
                'blog_id' : form.attr('name'),
                csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val(),

            },

            success: function(json) {
                var visitorLiked = json.dataLiked[1];
                var likeCounter = parseInt($(".like-counter").text());
                for(key in visitorLiked){
                    if(visitorLiked[key] == true){
                        // alert("Blog liked!");
                        
                        $("#like-btn").attr("src","https://www.flaticon.com/svg/static/icons/svg/535/535183.svg");
                        likeCounter += 1;
                        $(".like-counter").html(likeCounter);
                    }

                    else if (visitorLiked[key] == false){
                        // alert("Blog unliked!");
                        $("#like-btn").attr("src","https://www.flaticon.com/svg/static/icons/svg/1000/1000621.svg");
                        likeCounter -= 1;
                        $(".like-counter").html(likeCounter);
                    }
                }
            }
        });
    }

    function add_comment() {
        console.log("create comment is working!"); // sanity check
        var cform = $('#comment-form');
        var url_comment = cform.attr('url_path');
        
        $.ajax({
            url : url_comment,
            type : "POST",
            
            data : { 
                comment : $('#blog-comment').val(),
                csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val(),
            },

            success : function(json) {
                var comments = json.dataComment[0];
                $('#blog-comment').val('');
                $('.each-comment').empty();
                $('.intro-msg-comment').empty();
                $('.intro-msg-comment').append(
                    `<h1>Add your comment <span class="bold-red">here!</span></h1>`
                );
                $('.success-comment').append(
                    `<div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <h5>Comment added <strong style="font-size:inherit;">successfully!</strong></h5>
                    </div>`
                );
                for(var comment in comments){
                    $('.each-comment').append(
                        '<div class="comment">'+
                        '<h3><span class="bold-red">' + comments[comment][1] + '</span> - (' + comments[comment][2] + ')</h3>' +
                        '<p class="long-par">' + comments[comment][0] + '</p>' +
                        '</div>'  
                    );
                }
                //console.log(json);
                console.log("success");
            },
        });
        
        //console.log($('#blog-comment').val());
    };

});

    

