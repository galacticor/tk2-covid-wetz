$(document).ready(function(){
    $('#form-feedback').on('submit', function(event){
        event.preventDefault();
        createFeedback();
        // console.log("ajax call ends");
    });

    $('a.prev').click(function(){
        var windowWidth = $(window).width();
        console.log(windowWidth);
        $('#slideshow-container').animate(
            {scrollLeft:  '-=' + String(windowWidth)},
            1000
        );
    });

    $('a.next').click(function(){
        var windowWidth = $(window).width();
        $('#slideshow-container').animate(
            {scrollLeft:  '+=' + String(windowWidth)},
            1000
        );
    });
});

function createFeedback(){
    $.ajax({
        url:"/about/feedbackProcess/",
        type:'POST',
        data:{
            feedbackData:$('#id_pesan').val(),
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
        },
        success:function(json){
            $('.wrapper-feedback').fadeOut(300);
            setTimeout(function(){
                $('#input-feedback').hide();
                $('#feedback-show').show();
            }, 300);
            setTimeout(function(){
                $('.has-submit').fadeIn(300);
            }, 600);
            var feedbacks = json.dataFeedback[0];
            for(var key in feedbacks){
                $('#slideshow-container').prepend(
                    '<div class="mySlides">' +
                        '<div class="feedback-container">' +
                            '<h3 class="pesan">' + feedbacks[key] + '</h3>' +
                            '<h5 class="pemberi-pesan">' + key + '</h5>' +
                        '</div>' +
                    '</div>'
                    );
                    // $('#slideshow-container').eq(0).addClass('onshow');
                    //console.log(key + " : " + feedbacks[key]);
            };
            // $('#slideshow-container').children().eq(0).addClass('onshow');
            // console.log("sanity check");
        },
        error:function(){
            console.log("Error");
        }
    });
};