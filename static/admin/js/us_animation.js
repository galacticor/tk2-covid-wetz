function title11(item){
    // Wrap every letter in a span
    var textWrapper = $("."+ item.get(0).className +' .letters');
    textWrapper.html(function (i, html) {
        return html.replace(/\S/g, "<span class='letter'>$&</span>");
    });
    // console.log("called");
    var className = "";
    for(i = 0; i < item.length; i++){
        className += "." + item[i].className;
    }
    
    anime.timeline()
    .add({
        targets: className + ' .line',
        scaleY: [0,1],
        opacity: [0.5,1],
        easing: "easeOutExpo",
        duration: 700
    })
    .add({
        targets: className + ' .line',
        translateX: [0, $(className + ' .letters').get(0).getBoundingClientRect().width + 10],
        easing: "easeOutExpo",
        duration: 700,
        delay: 100
    }).add({
        targets:  className + ' .letter',
        opacity: [0,1],
        easing: "easeOutExpo",
        duration: 600,
        offset: '-=775',
        delay: (el, i) => 100 * (i+1)
    }).add({
        targets: className + ' .line',
        opacity: 0,
        duration: 1000,
        easing: "easeOutExpo",
        delay: 1000
    });
}

function rotateIcon(){
    const tl = gsap.timeline();
    tl.fromTo(
        $('#icon-us'),
        2,
        {
            opacity:0, width:"50%", height:"50%"
        },
        {
            opacity:1, width:"100%", height:"100%",
            ease: Power1.easeInOut
        },
    )
    .to(
        $('#icon-us'),
        1.5,
        {rotation:"180", ease: Power1.easeInOut},
        "-=2"
    );
};

function authorRightAnimation(part){
    $(part).css({
        'background-color': 'black',
        transition: 'background-color 0.4s ease-in-out',
    });
    const tl = gsap.timeline();
    var partID = "#" + part.attr('id') + " ";
    setTimeout(function(){
        var text = $(part).find('.right-author').children().not('.button-right');
        var nick = ".author-nick-right";
        var name = ".author-name-right";
        var desc = ".author-desc-right";
        var image = '.author-photo';
        var button = '.button-right';
        tl.fromTo(
            $(partID+nick),
            1,
            {opacity: 0, x: -30},
            {opacity: 1, x:0, ease:Power1.easeInOut},
        ).fromTo(
            $(partID+name),
            1,
            {opacity: 0, x: -30},
            {opacity: 1, x:0, ease:Power1.easeInOut},
            '-=0.75'
        ).fromTo(
            $(partID+desc),
            1,
            {opacity: 0, x: -30},
            {opacity: 1, x:0, ease:Power1.easeInOut},
            '-=0.75'
        ).fromTo(
            $(partID+image),
            1,
            {opacity: 0, x: +30},
            {opacity: 1, x: 0, ease:Power1.easeInOut},
            '-=1.75',
        ).fromTo(
            $(partID+button),
            0.75,
            {opacity: 0},
            {opacity: 1},
        ).fromTo(
            $(partID+button + " > button"),
            0.75,
            {backgroundColor:"#DE4F22", color:'white'},
            {backgroundColor:"#FFD162", color:'black'},
            '-=0.75'
        );
    }, 500);
}

function authorLeftAnimation(part){
    $(part).css({
        'background-color': '#FFD162',
        transition: 'background-color 0.5s ease-in-out',
    });
    const tl = gsap.timeline();
    var partID = "#" + part.attr('id') + " ";
    setTimeout(function(){
        var nick = ".author-nick-left";
        var name = ".author-name-left";
        var desc = ".author-desc-left";
        var image = '.author-photo';
        var button = '.button-left';
        tl.fromTo(
            $(partID+nick),
            1,
            {opacity: 0, x: +30},
            {opacity: 1, x:0, ease:Power1.easeInOut},
        ).fromTo(
            $(partID+name),
            1,
            {opacity: 0, x: +30},
            {opacity: 1, x:0, ease:Power1.easeInOut},
            '-=0.75'
        ).fromTo(
            $(partID+desc),
            1,
            {opacity: 0, x: +30},
            {opacity: 1, x:0, ease:Power1.easeInOut},
            '-=0.75'
        ).fromTo(
            $(partID+image),
            1,
            {opacity: 0, x: -30},
            {opacity: 1, x: 0, ease:Power1.easeInOut},
            '-=1.75',
        ).fromTo(
            $(partID+button),
            0.75,
            {opacity: 0},
            {opacity: 1},
        ).fromTo(
            $(partID+button + " > button"),
            0.75,
            {backgroundColor:"#FFD162", color:'black'},
            {backgroundColor:"#DE4F22", color:'white'},
            '-=0.75'
        );
    }, 500);
}

function isScrolledIntoView($elem, $window) {
    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom  - 75 <= docViewBottom) && (elemTop + 75 >= docViewTop));
}

$(document).ready(function(){
    rotateIcon();

    setTimeout(function(){
        $('.intro-title').css('opacity', 1);
        title11($('.intro-title'));
    }, 1000);

    setTimeout(function(){
        $('#typed-string-initial').animate(
            {opacity:'1'},
            'slow'
        );
    }, 3000);

    var scrolledEba = false;
    var scrolledTimi = false;
    var scrolledWidi = false;
    var scrolledZul = false;

    $(document).scroll(function(){
        if(isScrolledIntoView($('#author-eba'), $(window)) && !scrolledEba){
            scrolledEba = true;
            authorRightAnimation($('#author-eba'));
        };
        if(isScrolledIntoView($('#author-timi'), $(window)) && !scrolledTimi){
            scrolledTimi = true;
            authorLeftAnimation($('#author-timi'));
        };
        if(isScrolledIntoView($('#author-widi'), $(window)) && !scrolledWidi){
            scrolledWidi = true;
            authorRightAnimation($('#author-widi'));
        };
        if(isScrolledIntoView($('#author-zul'), $(window)) && !scrolledZul){
            scrolledZul = true;
            authorLeftAnimation($('#author-zul'));
        };
    })
});