from django import forms

class UserForm(forms.Form):
    name = forms.CharField(
        max_length=20,
        widget=forms.TextInput(attrs={
            'class':'inputText',
            'placeholder': 'Your name here ...'
            })
        )

    class Meta:
        fields = ['name']