from django.db import models

# Create your models here.
class Name(models.Model):
    name = models.CharField(default="y/n", max_length = 25, blank=True, null=True)

    def __str__(self):
        return self.name
    