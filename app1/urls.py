from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name="index"),
    path('guest', views.index_guest, name="guest")
]