from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
from .forms import *
from django.contrib.auth.models import User, auth
from app4.models import Poll
# Create your views here. mau commit lagi


def index_guest(request):
	f = UserForm(request.POST or None)
	context = {
		'form': f,
	}
	if request.method == 'POST':
		name = request.POST.get('name')
		context = {
			'name' : name,
		}
		r =  redirect('/')
		r.set_cookie('name', name)
		request.session['name'] = name 
		nama = Name(name=name)
		nama.save()
		return r

	name = request.COOKIES.get('name')
	context['name'] = name
		
	return render(request, "login_guest.html", context)

def index(request):
  msg = request.session.get('msg', None)
  # print(msg)
  try:
    del request.session['msg']
  except:
    pass
  
  name = request.COOKIES.get('name')
  context = {
		'msg' : msg,
    'name' : name
  }
  return render(request, 'home.html', context)


def register(request):
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
       	password = request.POST['password']

       	hasil = "Registration Complete"
       	if User.objects.filter(username=email).exists() :
       		hasil = "Email already exist, try login instead"
       	else :
          user = User.objects.create_user(username=email, email=email, password=password, first_name=name)
          user.save()
          poll = Poll(user=user)
          poll.save()
          hasil = "Registration Complete, now you may Login"

        request.session['msg'] = hasil
        
    r = redirect('/')   
    r.delete_cookie('name') 
    return r

def login(request):
    if request.method == 'POST':
        username = request.POST['email']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)

        hasil = ''
        if user is not None:
            auth.login(request, user)
            request.session.set_expiry(300) # auto logout dalam 5 menit
            hasil = 'Login success'
        else :
        	hasil = "Wrong Username or Password"

        request.session['msg'] = hasil
    
    # print(hasil)
    r = redirect('/')   
    r.delete_cookie('name') 
    return r

def logout(request):
    auth.logout(request)
    r = redirect('/')  
    for c in request.COOKIES:
      r.delete_cookie(c) 
    return r
