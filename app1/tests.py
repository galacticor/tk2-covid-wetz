from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest, JsonResponse
from .views import *
from .models import *

# Create your tests here.
class TestGeneral(TestCase):
	def test_event_url_is_exist(self):
		response = Client().get("/")
		self.assertEqual(response.status_code, 200)

	def test_event_post_is_exist(self):
		r= Client().post("/")
		self.assertEqual(r.status_code, 200)

	# def test_event_post_cookies_is_worked(self):
	# 	data = {
	# 		'name' : "abc",
	# 	}
	# 	r= self.client.post("/guest/", data)
	# 	self.assertEqual(r.client.cookies.get('name').value, 'abc')


class TestLogin(TestCase):
	def setUp(self):
		user = User.objects.create_user(username='a@a.com', email='a@a.com', password='password', first_name='name')

	def test_url_post_registration_exist(self):
		data = {
			'name' : "Test",
			'email' : 'test@test.com',
			'password' : 'password'
		}
		response = Client().post('/register/', data=data)
		self.assertEqual(response.status_code, 302)

	def test_url_registration_already_exist(self):
		data = {
			'name' : "Test",
			'email' : 'a@a.com',
			'password' : 'password'
		}
		response = Client().post('/register/', data=data)
		self.assertEqual(response.status_code, 302)

	def test_url_post_login_success(self):
		data = {
			'email' : "a@a.com",
			'password' : 'password'
		}
		response = Client().post('/login/', data)
		self.assertEqual(response.status_code, 302)

	def test_url_post_login_failed(self):
		data = {
			'email' : "a@a.com",
			'password' : 'passwords'
		}
		response = Client().post('/login/', data)
		self.assertEqual(response.status_code, 302)

	def test_url_post_logout_exist(self):
		response = Client().post('/logout/')
		self.assertEqual(response.status_code, 302)

	def test_func_url_logout(self):
		found = resolve('/logout/')
		self.assertEqual(found.func, logout)

	def test_func_url_registration(self):
		found = resolve('/register/')
		self.assertEqual(found.func, register)
		
	def test_func_url_login(self):
		found = resolve('/login/')
		self.assertEqual(found.func, login)

