from django.db import models
from django.conf import settings


# Create your models here.
class Visitor(models.Model):
    name = models.CharField(default="-", max_length=100)

    def __str__(self):
        return self.name

class Author(models.Model):
    name = models.CharField(default="-", max_length=100)

    def __str__(self):
        return self.name


class Blog(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    author = models.ForeignKey(Author, related_name="author_blogs", on_delete=models.CASCADE, null=True, blank=True)
    likes = models.ManyToManyField(Visitor, related_name="blog_likes", blank=True)

    def __str__(self):
        return self.title

    def blogTotalLikes(self):
        return self.likes.all().count()

    def blogTotalComments(self):
        return self.blog_comments.all().count()

class Comment(models.Model):
    message = models.CharField(max_length=500, null=True)
    commenter = models.ForeignKey(Visitor, related_name="visitor_comment", on_delete=models.SET_NULL, null=True, blank=True)
    date_created = models.DateField(auto_now=True)
    blog_commented = models.ForeignKey(Blog, related_name="blog_comments", on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return "{} - {}".format(self.commenter, self.message)