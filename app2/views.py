from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
from .forms import *
from django.contrib.auth.models import User
from django.http import JsonResponse
import json

# Create your views here.


def index(request):
	blogs = Blog.objects.all()
	form = BlogForm()
	return render(request, 'app2/blog.html', {'blogs': blogs, 'form': form})

def add_blog(request):
	if request.method == "POST":

		title = request.POST.get('title')
		content = request.POST.get('content')

	
		visitor, created = Visitor.objects.get_or_create(name=request.user)

		author, created = Author.objects.get_or_create(name=request.user)

		newBlog = Blog.objects.create(title=title, content=content, author=author)

		listBlog = { blog.id : [blog.title, blog.content, blog.author.name] for blog in Blog.objects.all() }

		dictBlog = json.loads(json.dumps({'dataBlog' : [listBlog]}))

		return JsonResponse(dictBlog, safe=False)

def blog_content(request, title):
	blogs = Blog.objects.all()
		
	blog = Blog.objects.get(title=title)
	comment_form = CommentForm()

	visitor, created = Visitor.objects.get_or_create(name=request.user)

	if blog.likes.filter(name=visitor).exists():
		liked = True

	else:
		liked = False

	# if request.method == "POST":
		
	# 	comment_form = CommentForm(request.POST)
	# 	if comment_form.is_valid():
	# 		dataComment = comment_form.cleaned_data
	# 		msg = dataComment['message']
	# 		if request.COOKIES.get('name') == None:
	# 			prsn = "Anonymous"
	# 		else:
	# 		 	prsn = request.COOKIES.get('name')
	# 		blog.comment.create(message=msg, commenter=request.user)
	# 		blog.save()
	# 		return redirect('/blogs/')
	
	context = {'blog': blog, 'cform': comment_form, "liked":liked}

	return render(request, 'app2/blog_content.html', context)

	

def add_comment(request, title):
	blog = Blog.objects.get(title=title)
	if request.method == 'POST':
		comment = request.POST.get('comment')
		
		visitor, created = Visitor.objects.get_or_create(name=request.user)

		newComment = Comment.objects.create(message=comment, commenter=visitor, blog_commented=blog)

		listComment = { comment.id : [comment.message, comment.commenter.name, comment.date_created.strftime("%b %d, %Y")] for comment in blog.blog_comments.all() }

		dictComment = json.loads(json.dumps({'dataComment' : [listComment]}))
		
		return JsonResponse(dictComment, safe=False)
		# return JsonResponse(dictComment, safe=False)

def blog_like(request, title):
	blog = Blog.objects.get(title=title)
	
	if request.method == "POST":
		blog_id = request.POST.get('blog_id')

		visitor, created = Visitor.objects.get_or_create(name=request.user)

		if blog.likes.filter(name=visitor).exists():
			blog.likes.remove(visitor)
			liked = False
			
		else:
			blog.likes.add(visitor)
			liked = True
			
		listLiked = { blog_id : [visitor.name] for visitor in blog.likes.all() }
		# print(listLiked)
		visitorLiked = { 'liked' : liked }
		# print(visitorLiked)
		dictLiked = json.loads(json.dumps({'dataLiked' : [listLiked, visitorLiked]}))

		return JsonResponse(dictLiked, safe=False)