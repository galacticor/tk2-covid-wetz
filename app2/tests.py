from django.contrib import auth
from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from django.contrib.auth.models import User
from .views import *
from .models import *

# Create your tests here.
class TestIndex(TestCase):
	def test_event_url_is_exist(self):
		response = Client().get("/blogs/")
		self.assertEqual(response.status_code, 200)

	def test_event_is_using_index_function(self):
		found = resolve("/blogs/")
		self.assertEqual(found.func, index)

	def test_event_is_using_template(self):
		response = Client().get("/blogs/")
		self.assertTemplateUsed(response, 'app2/blog.html')

	def test_event_templates(self):
		response = Client().get('/blogs/')
		html_response = response.content.decode('utf8')
		self.assertIn("blogs", html_response)
		self.assertIn("things you might want to know", html_response)

	def test_event_templates_if_user_is_not_authenticated(self):
		response = Client().get('/blogs/')
		html_response = response.content.decode('utf8')
		self.assertIn("disabled", html_response)
		self.assertIn("Sorry, you have to log in to use this feature.", html_response)

class TestAddBlog(TestCase):
	def setUp(self):
		self.credentials = {
			'username': 'testuser',
			'email': 'test@gmail.com',
			'password': 'secret',
			'first_name': 'testuser'}
		User.objects.create_user(**self.credentials)

	def test_user_can_access_form_to_add_blog(self):
		self.client.login(username= 'testuser', email='test@gmail.com', password='secret')
		response = self.client.get('/blogs/', follow=True)
		html_response = response.content.decode('utf8')
		self.assertIn('Hello, testuser', html_response)
		self.assertIn('<input class="btn btn-lg but-add" type="submit" value="Send">', html_response)

	def test_user_adding_blog(self):
		self.client.login(username= 'testuser', email='test@gmail.com', password='secret')
		response = self.client.get('/blogs/', follow=True)
		dataBlog = {
			'title' : 'test',
			'content' : 'halo123'
		}
		posted = self.client.post("/blogs/add-blog", dataBlog)
		self.assertEqual(posted.status_code, 301)

	def test_user_blog_is_saved_to_database(self):
		self.client.login(username='testuser', email='test@gmail.com', password='secret')
		response = self.client.get('/blogs/', follow=True)
		dataBlog = {
			'title' : 'test',
			'content' : 'halo123'
		}
		self.client.post("/blogs/add-blog/", dataBlog)
		self.assertEqual(Blog.objects.all().count(), 1)

	def test_added_blog_is_exist_in_index_page(self):
		self.client.login(username='testuser', email='test@gmail.com', password='secret')
		dataBlog = {
			'title' : 'test',
			'content' : 'halo123'
		}
		self.client.post("/blogs/add-blog/", dataBlog)
		response = self.client.get('/blogs/', follow=True)
		html_response = response.content.decode('utf8')
		self.assertIn('test', html_response)
		self.assertIn('halo123', html_response)

	def test_added_blog_details_can_be_accessed(self):
		self.client.login(username='testuser', email='test@gmail.com', password='secret')
		dataBlog = {
			'title' : 'test',
			'content' : 'halo123'
		}
		self.client.post("/blogs/add-blog/", dataBlog)
		response = self.client.get('/blogs/details/test', follow=True)
		html_response = response.content.decode('utf8')
		self.assertIn('test', html_response)
		self.assertIn('By:<br>testuser</h2>', html_response)

	def test_added_blog_is_using_blog_content_function(self):
		self.client.login(username='testuser', email='test@gmail.com', password='secret')
		dataBlog = {
			'title' : 'test',
			'content' : 'halo123'
		}
		self.client.post("/blogs/add-blog/", dataBlog)
		found = resolve('/blogs/details/test/')
		self.assertEqual(found.func, blog_content)

	def test_added_blog_details_url_is_exist(self):
		self.client.login(username='testuser', email='test@gmail.com', password='secret')
		dataBlog = {
			'title' : 'test',
			'content' : 'halo123'
		}
		self.client.post("/blogs/add-blog/", dataBlog)
		response = Client().get("/blogs/details/test")
		self.assertEqual(response.status_code, 301)

	def test_like_blog_url_is_working(self):
		self.client.login(username='testuser', email='test@gmail.com', password='secret')
		dataBlog = {
			'title' : 'test',
			'content' : 'halo123'
		}
		self.client.post("/blogs/add-blog/", dataBlog)
		response = self.client.post("/blogs/details/test/like/")
		self.assertEqual(response.status_code, 200)
		
	def test_like_blog_is_saving_the_like_to_database(self):
		self.client.login(username='testuser', email='test@gmail.com', password='secret')
		dataBlog = {
			'title' : 'test',
			'content' : 'halo123'
		}
		self.client.post("/blogs/add-blog/", dataBlog)
		response = self.client.post("/blogs/details/test/like/")
		blog = Blog.objects.get(title='test')
		self.assertEqual(blog.likes.all().count(), 1)

	def test_like_blog_counter_is_working(self):
		self.client.login(username='testuser', email='test@gmail.com', password='secret')
		dataBlog = {
			'title' : 'test',
			'content' : 'halo123'
		}
		self.client.post("/blogs/add-blog/", dataBlog)
		self.client.post("/blogs/details/test/like/")
		response = self.client.get('/blogs/details/test', follow=True)
		html_response = response.content.decode('utf8')
		self.assertIn('1', html_response)

	def test_unlike_blog_counter_is_working(self):
		self.client.login(username='testuser', email='test@gmail.com', password='secret')
		dataBlog = {
			'title' : 'test',
			'content' : 'halo123'
		}
		self.client.post("/blogs/add-blog/", dataBlog)
		self.client.post("/blogs/details/test/like/")
		self.client.post("/blogs/details/test/like/")
		response = self.client.get('/blogs/details/test', follow=True)
		html_response = response.content.decode('utf8')
		self.assertIn('0', html_response)

	def test_add_comment_url_is_working(self):
		self.client.login(username='testuser', email='test@gmail.com', password='secret')
		dataBlog = {
			'title' : 'test',
			'content' : 'halo123'
		}
		self.client.post("/blogs/add-blog/", dataBlog)
		response = self.client.post("/blogs/details/test/add-comment/", {'message' : 'testkomen'})
		self.assertEqual(response.status_code, 200)

	def test_add_comment_is_saving_comment(self):
		self.client.login(username='testuser', email='test@gmail.com', password='secret')
		dataBlog = {
			'title' : 'test',
			'content' : 'halo123'
		}
		self.client.post("/blogs/add-blog/", dataBlog)
		self.client.post("/blogs/details/test/add-comment/", {'message' : 'testkomen'})
		response = self.client.get('/blogs/details/test', follow=True)
		blog = Blog.objects.get(title='test')
		self.assertEqual(blog.blog_comments.all().count(), 1)

	def test_comment_is_added_to_the_blog_details_page(self):
		self.client.login(username='testuser', email='test@gmail.com', password='secret')
		dataBlog = {
			'title' : 'test',
			'content' : 'halo123'
		}
		self.client.post("/blogs/add-blog/", dataBlog)
		self.client.post("/blogs/details/test/add-comment/", {'message' : 'testkomen'})
		response = self.client.get('/blogs/details/test', follow=True)
		
		html_response = response.content.decode('utf8')
		self.assertIn('testuser', html_response)