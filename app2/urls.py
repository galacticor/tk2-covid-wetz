from django.urls import path
from . import views

app_name = 'blogs'

urlpatterns = [
    path('', views.index, name="index"),
    path('add-blog/', views.add_blog, name="add_blog"),
    path('details/<str:title>/', views.blog_content, name="blog_content"),
    path('details/<str:title>/like/', views.blog_like, name="like"),
    path('details/<str:title>/add-comment/', views.add_comment, name="add_comment")
]