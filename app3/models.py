from django.db import models

# Create your models here.

class Feedback(models.Model):
    pesan = models.TextField(null=True, blank=False)
    nama = models.TextField(null=True, blank=False, default="Anonymous")

    def __str__(self):
        return self.pesan