from django import forms
from django.forms import ModelForm
from .models import Feedback

class FeedbackForm(ModelForm):
    class Meta:
        model = Feedback
        fields = ['pesan']
        widgets = {
            'pesan' : forms.Textarea(attrs={
                'cols':80,
                'class' : 'form-control mx-auto border rounded-lg',
                'placeholder' : 'Type here...',
                'required' : 'true'})
            }