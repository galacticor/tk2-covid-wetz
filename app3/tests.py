from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import index
from .models import *
from django.contrib.auth.models import User
  
# Modules for testing session

from django.conf import settings as django_settings
from django.test import TestCase
from importlib import import_module

# Setup session test, credit: https://gist.github.com/dustinfarris/4982145

class SessionEnabledTestCase(TestCase):

    def get_session(self):
        if self.client.session:
            session = self.client.session
        else:
            engine = import_module(django_settings.SESSION_ENGINE)
            session = engine.SessionStore()
        return session

    def set_session_cookies(self, session):
        # Set the cookie to represent the session
        session_cookie = django_settings.SESSION_COOKIE_NAME
        self.client.cookies[session_cookie] = session.session_key
        cookie_data = {
            'max-age': None,
            'path': '/',
            'domain': django_settings.SESSION_COOKIE_DOMAIN,
            'secure': django_settings.SESSION_COOKIE_SECURE or None,
            'expires': None}
        self.client.cookies[session_cookie].update(cookie_data)


# Create your tests here.
class App3Test(TestCase):
    def test_about_url_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)
    
    def test_about_using_about_us_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'app3/about_us.html')
    
    def test_about_using_index_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, index)
    
    def test_about_text_in_html(self):
        response = Client().get('/about/')
        html_response = response.content.decode('utf8')
        self.assertIn("US.", html_response)
        self.assertIn("here\'s our <span>team</span>,<br>desparetely need more <span>sleep", html_response)
        self.assertIn("EBA", html_response)
        self.assertIn("Eba Ginting", html_response)
        self.assertIn("\"it's just three letters. no mispell is allowed.\"", html_response)
        self.assertIn("TIMMY", html_response)
        self.assertIn("Timothy Efraim", html_response)
        self.assertIn("\"heccing hecc.\"", html_response)
        self.assertIn("Widy", html_response)
        self.assertIn("Widyanto Hadi", html_response)
        self.assertIn("\"Jangan kebanyakan belajar\"", html_response)
        self.assertIn("Zul", html_response)
        self.assertIn("Zulfahri Haradi", html_response)
        self.assertIn("\"Roses are red, violets are blue, ppw is pain\"", html_response)
        self.assertIn("Got a <span>feedback</span> for us? Let us <span>hear!", html_response)

class App3ModelTest(TestCase):
    def test_feedback_model_exist(self):
        Feedback.objects.create(pesan="test")
        self.assertEqual(Feedback.objects.all().count(), 1)

    def test_cannot_add_empty_feedback(self):
        Client().post('/about/', {})
        self.assertNotEqual(Feedback.objects.all().count(), 1)

class App3FeedbackTest(TestCase):    
    def test_feedback_part_exist(self):
        response = Client().post('/about/', {'pesan':'pesan masuk'})
        html_response = response.content.decode('utf8')
        self.assertIn("Thanks for your <span>feedback!</span>", html_response)

class App3TestAsGuest(SessionEnabledTestCase):
    def test_cannot_input_feedback(self):
        response = Client().get('/about/')
        html_response = response.content.decode('utf8')
        self.assertIn("Sorry, you have to log in to use this feature", html_response)

    def test_with_session_named_guest(self):
        session = self.get_session()
        session['name'] = 'guest'
        session.save()

        self.set_session_cookies(session)

        response = self.client.get('/about/')
        html_response = response.content.decode('utf8')

        self.assertIn(('name', 'guest'), response.client.session.items())
        self.assertIn("Hello, guest", html_response)

class App3TestAsUser(TestCase):
    def test_hi_username(self):
        user = User.objects.create_user(
            username="username",
            first_name="username",
            email="user@email.com",
            password="user_password",
            )

        self.client.login(username="username", password="user_password")

        response = self.client.get('/about/')
        html_response = response.content.decode('utf8')

        self.assertIn("Hello, username", html_response)
    
    def test_can_add_feedback(self):
        user = User.objects.create_user(
            username="username",
            first_name="username",
            email="user@email.com",
            password="user_password",
            )

        self.client.login(username="username", password="user_password")

        response = self.client.get('/about/')
        html_response = response.content.decode('utf8')
        self.assertNotIn("Sorry, you have to log in to use this feature", html_response)
    
    def test_ajax_views_works(self):
        user = User.objects.create_user(
            username="username",
            first_name="username",
            email="user@email.com",
            password="user_password",
            )

        self.client.login(username="username", password="user_password")
        
        response = self.client.post(reverse('about:feedbackProcess'), {'feedbackData' : 'pesan'})
        for key in response.json()['dataFeedback'][0].keys():
            self.assertEqual(key, 'username')
        self.assertEqual(response.json()['dataFeedback'][0]['username'], 'pesan')
    
    def test_feedback_with_name_already_exist(self):
        Feedback.objects.create(pesan="test", nama="username")
        user = User.objects.create_user(
            username="username",
            first_name="username",
            email="user@email.com",
            password="user_password",
            )

        self.client.login(username="username", password="user_password")
        
        response = self.client.post(reverse('about:feedbackProcess'), {'feedbackData' : 'pesan'})
        for key in response.json()['dataFeedback'][0].keys():
            self.assertEqual(key, 'username')
        self.assertEqual(response.json()['dataFeedback'][0]['username'], 'pesan')


