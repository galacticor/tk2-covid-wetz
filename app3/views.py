from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Feedback
from .forms import FeedbackForm
import json
from django.http import JsonResponse

# Create your views here.



def index(request):
	form = FeedbackForm()
	context = {
		'form' : form,
		}
	return render(request, 'app3/about_us.html', context)

def feedbackProcess(request):
	feedback = request.POST.get('feedbackData')
	username = request.user.first_name
	
	tempObj = Feedback.objects.filter(nama=username).first()
	if tempObj is None:
		new_feed = Feedback(pesan=feedback, nama=username)
		new_feed.save()
	else:
		tempObj.pesan = feedback
		tempObj.save()

	list_feedback = { feedback.nama : feedback.pesan for feedback in Feedback.objects.all()}

	dicti = json.loads(json.dumps({'dataFeedback' : [list_feedback]}))

	return JsonResponse(dicti, safe=False)

